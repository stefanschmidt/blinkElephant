//
// Created by shmddl on 10.12.16.
//

#include "Arduino.h"

#define LED_PIN 13
#define MIC_PIN 2

int sensorValue = 0;

void setup() {
    pinMode(LED_PIN, OUTPUT);
    pinMode(MIC_PIN, INPUT);
    Serial.begin(9600);
}

void loop() {
    sensorValue = analogRead(A0);
    Serial.println(sensorValue);
    digitalWrite(LED_PIN, digitalRead(MIC_PIN)); 
    delay(100);
}
