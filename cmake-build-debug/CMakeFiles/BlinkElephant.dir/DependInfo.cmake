# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/shmddl/code/Arduino/BlinkElephant/src/main.cpp" "/home/shmddl/code/Arduino/BlinkElephant/cmake-build-debug/CMakeFiles/BlinkElephant.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ARDUINO=10612"
  "ARDUINO_ARCH_AVR"
  "ARDUINO_AVR_NANO"
  "F_CPU=16000000L"
  "PLATFORMIO=30201"
  "__AVR_ATmega328P__"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/shmddl/.platformio/packages/framework-arduinoavr/cores/arduino"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/variants/eightanaloginputs"
  "../src"
  "/home/shmddl/.platformio/lib/Adafruit_NeoPixel_ID28"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/Bridge/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/EEPROM/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/Esplora/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/Ethernet/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/Firmata"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/Firmata/utility"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/GSM/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/HID/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/Keyboard/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/LiquidCrystal/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/Mouse/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/RobotIRremote/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/Robot_Control/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/Robot_Motor/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/SD/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/SPI/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/SPI1/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/Scheduler/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/Servo/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/SoftwareSerial/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/SpacebrewYun/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/Stepper/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/TFT/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/Temboo/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/USBHost/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/WiFi/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/Wire/src"
  "/home/shmddl/.platformio/packages/framework-arduinoavr/libraries/Wire1/src"
  "/home/shmddl/.platformio/packages/toolchain-atmelavr/avr/include"
  "/home/shmddl/.platformio/packages/toolchain-atmelavr/lib/gcc/avr/4.9.2/include-fixed"
  "/home/shmddl/.platformio/packages/toolchain-atmelavr/lib/gcc/avr/4.9.2/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
